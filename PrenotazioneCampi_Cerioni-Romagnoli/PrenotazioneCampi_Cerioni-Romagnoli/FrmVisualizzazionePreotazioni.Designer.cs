﻿namespace PrenotazioneCampi_Cerioni_Romagnoli
{
    partial class FrmVisualizzazionePreotazioni
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.rb1920 = new System.Windows.Forms.RadioButton();
            this.rb1819 = new System.Windows.Forms.RadioButton();
            this.rb1718 = new System.Windows.Forms.RadioButton();
            this.rb1617 = new System.Windows.Forms.RadioButton();
            this.rb1516 = new System.Windows.Forms.RadioButton();
            this.rb1415 = new System.Windows.Forms.RadioButton();
            this.rb1314 = new System.Windows.Forms.RadioButton();
            this.rb1213 = new System.Windows.Forms.RadioButton();
            this.rb1112 = new System.Windows.Forms.RadioButton();
            this.rb1011 = new System.Windows.Forms.RadioButton();
            this.rb910 = new System.Windows.Forms.RadioButton();
            this.rb89 = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.dtmData = new System.Windows.Forms.DateTimePicker();
            this.lvPrenotazioni = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.rbTutte = new System.Windows.Forms.RadioButton();
            this.tmVerifica = new System.Windows.Forms.Timer(this.components);
            this.btCancella = new System.Windows.Forms.Button();
            this.btModifica = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rb1920
            // 
            this.rb1920.AutoSize = true;
            this.rb1920.Location = new System.Drawing.Point(9, 276);
            this.rb1920.Margin = new System.Windows.Forms.Padding(2);
            this.rb1920.Name = "rb1920";
            this.rb1920.Size = new System.Drawing.Size(82, 17);
            this.rb1920.TabIndex = 66;
            this.rb1920.TabStop = true;
            this.rb1920.Text = "19:00-20:00";
            this.rb1920.UseVisualStyleBackColor = true;
            this.rb1920.CheckedChanged += new System.EventHandler(this.rb1920_CheckedChanged);
            // 
            // rb1819
            // 
            this.rb1819.AutoSize = true;
            this.rb1819.Location = new System.Drawing.Point(9, 254);
            this.rb1819.Margin = new System.Windows.Forms.Padding(2);
            this.rb1819.Name = "rb1819";
            this.rb1819.Size = new System.Drawing.Size(82, 17);
            this.rb1819.TabIndex = 65;
            this.rb1819.TabStop = true;
            this.rb1819.Text = "18:00-19:00";
            this.rb1819.UseVisualStyleBackColor = true;
            this.rb1819.CheckedChanged += new System.EventHandler(this.rb1819_CheckedChanged);
            // 
            // rb1718
            // 
            this.rb1718.AutoSize = true;
            this.rb1718.Location = new System.Drawing.Point(9, 232);
            this.rb1718.Margin = new System.Windows.Forms.Padding(2);
            this.rb1718.Name = "rb1718";
            this.rb1718.Size = new System.Drawing.Size(82, 17);
            this.rb1718.TabIndex = 64;
            this.rb1718.TabStop = true;
            this.rb1718.Text = "17:00-18:00";
            this.rb1718.UseVisualStyleBackColor = true;
            this.rb1718.CheckedChanged += new System.EventHandler(this.rb1718_CheckedChanged);
            // 
            // rb1617
            // 
            this.rb1617.AutoSize = true;
            this.rb1617.Location = new System.Drawing.Point(9, 210);
            this.rb1617.Margin = new System.Windows.Forms.Padding(2);
            this.rb1617.Name = "rb1617";
            this.rb1617.Size = new System.Drawing.Size(82, 17);
            this.rb1617.TabIndex = 63;
            this.rb1617.TabStop = true;
            this.rb1617.Text = "16:00-17:00";
            this.rb1617.UseVisualStyleBackColor = true;
            this.rb1617.CheckedChanged += new System.EventHandler(this.rb1617_CheckedChanged);
            // 
            // rb1516
            // 
            this.rb1516.AutoSize = true;
            this.rb1516.Location = new System.Drawing.Point(9, 188);
            this.rb1516.Margin = new System.Windows.Forms.Padding(2);
            this.rb1516.Name = "rb1516";
            this.rb1516.Size = new System.Drawing.Size(82, 17);
            this.rb1516.TabIndex = 62;
            this.rb1516.TabStop = true;
            this.rb1516.Text = "15:00-16:00";
            this.rb1516.UseVisualStyleBackColor = true;
            this.rb1516.CheckedChanged += new System.EventHandler(this.rb1516_CheckedChanged);
            // 
            // rb1415
            // 
            this.rb1415.AutoSize = true;
            this.rb1415.Location = new System.Drawing.Point(9, 167);
            this.rb1415.Margin = new System.Windows.Forms.Padding(2);
            this.rb1415.Name = "rb1415";
            this.rb1415.Size = new System.Drawing.Size(82, 17);
            this.rb1415.TabIndex = 61;
            this.rb1415.TabStop = true;
            this.rb1415.Text = "14:00-15:00";
            this.rb1415.UseVisualStyleBackColor = true;
            this.rb1415.CheckedChanged += new System.EventHandler(this.rb1415_CheckedChanged);
            // 
            // rb1314
            // 
            this.rb1314.AutoSize = true;
            this.rb1314.Location = new System.Drawing.Point(9, 145);
            this.rb1314.Margin = new System.Windows.Forms.Padding(2);
            this.rb1314.Name = "rb1314";
            this.rb1314.Size = new System.Drawing.Size(82, 17);
            this.rb1314.TabIndex = 60;
            this.rb1314.TabStop = true;
            this.rb1314.Text = "13:00-14:00";
            this.rb1314.UseVisualStyleBackColor = true;
            this.rb1314.CheckedChanged += new System.EventHandler(this.rb1314_CheckedChanged);
            // 
            // rb1213
            // 
            this.rb1213.AutoSize = true;
            this.rb1213.Location = new System.Drawing.Point(9, 123);
            this.rb1213.Margin = new System.Windows.Forms.Padding(2);
            this.rb1213.Name = "rb1213";
            this.rb1213.Size = new System.Drawing.Size(82, 17);
            this.rb1213.TabIndex = 59;
            this.rb1213.TabStop = true;
            this.rb1213.Text = "12:00-13:00";
            this.rb1213.UseVisualStyleBackColor = true;
            this.rb1213.CheckedChanged += new System.EventHandler(this.rb1213_CheckedChanged);
            // 
            // rb1112
            // 
            this.rb1112.AutoSize = true;
            this.rb1112.Location = new System.Drawing.Point(9, 101);
            this.rb1112.Margin = new System.Windows.Forms.Padding(2);
            this.rb1112.Name = "rb1112";
            this.rb1112.Size = new System.Drawing.Size(82, 17);
            this.rb1112.TabIndex = 58;
            this.rb1112.TabStop = true;
            this.rb1112.Text = "11:00-12:00";
            this.rb1112.UseVisualStyleBackColor = true;
            this.rb1112.CheckedChanged += new System.EventHandler(this.rb1112_CheckedChanged);
            // 
            // rb1011
            // 
            this.rb1011.AutoSize = true;
            this.rb1011.Location = new System.Drawing.Point(9, 79);
            this.rb1011.Margin = new System.Windows.Forms.Padding(2);
            this.rb1011.Name = "rb1011";
            this.rb1011.Size = new System.Drawing.Size(82, 17);
            this.rb1011.TabIndex = 57;
            this.rb1011.TabStop = true;
            this.rb1011.Text = "10:00-11:00";
            this.rb1011.UseVisualStyleBackColor = true;
            this.rb1011.CheckedChanged += new System.EventHandler(this.rb1011_CheckedChanged);
            // 
            // rb910
            // 
            this.rb910.AutoSize = true;
            this.rb910.Location = new System.Drawing.Point(9, 57);
            this.rb910.Margin = new System.Windows.Forms.Padding(2);
            this.rb910.Name = "rb910";
            this.rb910.Size = new System.Drawing.Size(82, 17);
            this.rb910.TabIndex = 56;
            this.rb910.TabStop = true;
            this.rb910.Text = "09:00-10:00";
            this.rb910.UseVisualStyleBackColor = true;
            this.rb910.CheckedChanged += new System.EventHandler(this.rb910_CheckedChanged);
            // 
            // rb89
            // 
            this.rb89.AutoSize = true;
            this.rb89.Location = new System.Drawing.Point(9, 35);
            this.rb89.Margin = new System.Windows.Forms.Padding(2);
            this.rb89.Name = "rb89";
            this.rb89.Size = new System.Drawing.Size(82, 17);
            this.rb89.TabIndex = 55;
            this.rb89.TabStop = true;
            this.rb89.Text = "08:00-09:00";
            this.rb89.UseVisualStyleBackColor = true;
            this.rb89.CheckedChanged += new System.EventHandler(this.rb89_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 8);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 54;
            this.label4.Text = "giorno";
            // 
            // dtmData
            // 
            this.dtmData.Location = new System.Drawing.Point(49, 8);
            this.dtmData.Margin = new System.Windows.Forms.Padding(2);
            this.dtmData.Name = "dtmData";
            this.dtmData.Size = new System.Drawing.Size(151, 20);
            this.dtmData.TabIndex = 53;
            this.dtmData.ValueChanged += new System.EventHandler(this.dtmData_ValueChanged);
            // 
            // lvPrenotazioni
            // 
            this.lvPrenotazioni.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.lvPrenotazioni.HideSelection = false;
            this.lvPrenotazioni.Location = new System.Drawing.Point(93, 35);
            this.lvPrenotazioni.Margin = new System.Windows.Forms.Padding(2);
            this.lvPrenotazioni.Name = "lvPrenotazioni";
            this.lvPrenotazioni.Size = new System.Drawing.Size(486, 259);
            this.lvPrenotazioni.TabIndex = 67;
            this.lvPrenotazioni.UseCompatibleStateImageBehavior = false;
            this.lvPrenotazioni.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Campo";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Cliente";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "data";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Ora inizio";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "durata (in ore)";
            this.columnHeader5.Width = 128;
            // 
            // rbTutte
            // 
            this.rbTutte.AutoSize = true;
            this.rbTutte.Location = new System.Drawing.Point(250, 11);
            this.rbTutte.Margin = new System.Windows.Forms.Padding(2);
            this.rbTutte.Name = "rbTutte";
            this.rbTutte.Size = new System.Drawing.Size(50, 17);
            this.rbTutte.TabIndex = 68;
            this.rbTutte.TabStop = true;
            this.rbTutte.Text = "Tutte";
            this.rbTutte.UseVisualStyleBackColor = true;
            this.rbTutte.CheckedChanged += new System.EventHandler(this.rbTutte_CheckedChanged);
            // 
            // btCancella
            // 
            this.btCancella.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btCancella.Location = new System.Drawing.Point(596, 67);
            this.btCancella.Name = "btCancella";
            this.btCancella.Size = new System.Drawing.Size(92, 25);
            this.btCancella.TabIndex = 70;
            this.btCancella.Text = "CANCELLA";
            this.btCancella.UseVisualStyleBackColor = true;
            this.btCancella.Click += new System.EventHandler(this.btCancella_Click);
            // 
            // btModifica
            // 
            this.btModifica.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btModifica.Location = new System.Drawing.Point(596, 35);
            this.btModifica.Name = "btModifica";
            this.btModifica.Size = new System.Drawing.Size(92, 25);
            this.btModifica.TabIndex = 69;
            this.btModifica.Text = "MODIFICA";
            this.btModifica.UseVisualStyleBackColor = true;
            this.btModifica.Click += new System.EventHandler(this.btModifica_Click);
            // 
            // FrmVisualizzazionePreotazioni
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(695, 346);
            this.Controls.Add(this.btCancella);
            this.Controls.Add(this.btModifica);
            this.Controls.Add(this.rbTutte);
            this.Controls.Add(this.lvPrenotazioni);
            this.Controls.Add(this.rb1920);
            this.Controls.Add(this.rb1819);
            this.Controls.Add(this.rb1718);
            this.Controls.Add(this.rb1617);
            this.Controls.Add(this.rb1516);
            this.Controls.Add(this.rb1415);
            this.Controls.Add(this.rb1314);
            this.Controls.Add(this.rb1213);
            this.Controls.Add(this.rb1112);
            this.Controls.Add(this.rb1011);
            this.Controls.Add(this.rb910);
            this.Controls.Add(this.rb89);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dtmData);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmVisualizzazionePreotazioni";
            this.Text = "Visualizza Preotazioni";
            this.Load += new System.EventHandler(this.FrmVisualizzazionePreotazioni_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rb1920;
        private System.Windows.Forms.RadioButton rb1819;
        private System.Windows.Forms.RadioButton rb1718;
        private System.Windows.Forms.RadioButton rb1617;
        private System.Windows.Forms.RadioButton rb1516;
        private System.Windows.Forms.RadioButton rb1415;
        private System.Windows.Forms.RadioButton rb1314;
        private System.Windows.Forms.RadioButton rb1213;
        private System.Windows.Forms.RadioButton rb1112;
        private System.Windows.Forms.RadioButton rb1011;
        private System.Windows.Forms.RadioButton rb910;
        private System.Windows.Forms.RadioButton rb89;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtmData;
        private System.Windows.Forms.ListView lvPrenotazioni;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.RadioButton rbTutte;
        private System.Windows.Forms.Timer tmVerifica;
        private System.Windows.Forms.Button btCancella;
        private System.Windows.Forms.Button btModifica;
    }
}